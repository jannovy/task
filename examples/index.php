<?php

declare(strict_types=1);

use JanNovy\Task\HTTPRequestListener;

// Autoload classes via composer
require __DIR__ . '/../vendor/autoload.php';

// Run HTTPRequestListener
new HTTPRequestListener();