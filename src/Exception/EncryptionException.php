<?php

declare(strict_types=1);

namespace JanNovy\Task\Exception;

class EncryptionException extends \Exception
{
}