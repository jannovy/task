<?php

declare(strict_types=1);

namespace JanNovy\Task;

interface EncryptionInterface
{
	public function encrypt(string $plaintext, string $password): string;
	public function decrypt(string $ciphertext, string $password): string;
}