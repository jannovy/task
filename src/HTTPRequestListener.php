<?php

declare(strict_types=1);

namespace JanNovy\Task;

use JanNovy\Task\Exception\EncryptionException;

class HTTPRequestListener
{
	function __construct()
	{
		$response = [
			'code' => 400,
			'message' => 'Error'
		];

		$requestUri = explode('/', $_SERVER['REQUEST_URI']);

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && count($requestUri) === 2) {
			try {
				$encryption = new Encryption();

				switch (strtolower($requestUri[1])) {
					case 'encrypt':
						if (array_key_exists('text', $_POST) && array_key_exists('password', $_POST)) {
							$response = [
								'code' => 200,
								'message' => $encryption->encrypt($_POST['text'], $_POST['password'])
							];
						}
						break;
					case 'decrypt':
						if (array_key_exists('ciphertext', $_POST) && array_key_exists('password', $_POST)) {
							$response = [
								'code' => 200,
								'message' => $encryption->decrypt($_POST['ciphertext'], $_POST['password'])
							];
						}
						break;
				}

			} catch (EncryptionException $exception) {
				$response = [
					'code' => 400,
					'message' => $exception->getMessage()
				];
			}
		}

		header('Content-Type: application/json; charset=utf-8', false, $response['code']);
		echo json_encode($response);
		die;
	}
}