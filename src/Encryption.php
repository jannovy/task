<?php

declare(strict_types=1);

namespace JanNovy\Task;

use JanNovy\Task\Exception\EncryptionException;

class Encryption implements EncryptionInterface
{

	const CIPHER = 'aes-256-cbc';

	public function encrypt(string $text, string $password): string
	{
		if (empty($text)) {
			throw new EncryptionException('Plaintext is empty.');
		}

		if (empty($password)) {
			throw new EncryptionException('Password is empty.');
		}

		$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length(self::CIPHER));
		$ciphertext = openssl_encrypt($text, self::CIPHER, $password, OPENSSL_RAW_DATA, $iv);

		if (!$ciphertext) {
			throw new EncryptionException('Encryption failed.');
		}

		return base64_encode($iv . $ciphertext);
	}

	public function decrypt(string $ciphertext, string $password): string
	{
		if (empty($ciphertext)) {
			throw new EncryptionException('Ciphertext is empty.');
		}

		if (empty($password)) {
			throw new EncryptionException('Password is empty.');
		}

		$data = base64_decode($ciphertext);
		$iv = substr($data, 0, openssl_cipher_iv_length(self::CIPHER));
		$ciphertext = substr($data, openssl_cipher_iv_length(self::CIPHER));
		$plaintext = openssl_decrypt($ciphertext, self::CIPHER, $password, OPENSSL_RAW_DATA, $iv);

		if ($plaintext === false) {
			throw new EncryptionException('Decryption failed.');
		}

		return $plaintext;
	}
}