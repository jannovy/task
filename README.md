# A simple PHP library for encrypting and decrypting text with REST API endpoint

## Basic usage

For basic usage with build-in PHP server run HTTPRequestListener instance.

    use JanNovy\Task\HTTPRequestListener;

    // Autoload classes via composer
    require __DIR__ . '/../vendor/autoload.php';

    // Run HTTPRequestListener
    new HTTPRequestListener();

## Advance usage

If you decide to use another webserver and your own existing REST API, use the Encryption class:

    $encryption = new Encryption();
    
    // Encryption
    $encryption->encrypt(<text>, <password>);

    // Decryption
    $encryption->decrypt(<ciphertext>, <password>');

## Docker

Run this command to start application in docker container
> docker-compose up --build

## Tests
Run PHPUnit tests in docker instance 
> docker-compose run --rm app php vendor/bin/phpunit tests/EncryptionTest.php

## REST API

### Encrypting
        [POST]localhost:8089/encrypt


| Parameter |     Type      | Mandatory  |
|-----------|:-------------:|:----------:|
| text      |    string     |    yes     |
| password  |    string     |    yes     |

### Decryptig
        [POST] localhost:8089/decrypt

| Parameter |     Type      | Mandatory  |
|-----------|:-------------:|:----------:|
|ciphertext |    string     |    yes     |
| password  |    string     |    yes     |


## Postman
You can test API endopoints with Postman. Just import collection
> postman_collection.json