FROM php:8.1

WORKDIR /app

# nainstalujeme některé potřebné balíčky
RUN apt-get update && apt-get install -y \
    git \
    openssl \
    zip \
    unzip \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# nainstalujeme Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /app

# spustíme Composer
RUN composer install --no-interaction --no-ansi --no-scripts

# nastavíme příkaz pro spuštění aplikace
CMD ["php", "-S", "0.0.0.0:80", "-t", "/app/examples/"]