<?php

declare(strict_types=1);

namespace JanNovy\Task;

use PHPUnit\Framework\TestCase;

class EncryptionTest extends TestCase
{
	const MESSAGE = 'some secret text';

	const PASSWORD = 'topSecretPassword';

	public function testEncrypt()
	{
		$encryption = new Encryption();

		$encryptedMessage = $encryption->encrypt(self::MESSAGE, self::PASSWORD);

		$decryptedMessage = $encryption->decrypt($encryptedMessage, self::PASSWORD);

		$this->assertEquals(
			self::MESSAGE,
			$decryptedMessage
		);
	}
}